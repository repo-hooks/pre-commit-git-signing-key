#! /usr/bin/env sh

signing_key=$(git config user.signingKey)

if test -n "$signing_key"; then
    case "$signing_key" in
        ssh-rsa* | \
            ecdsa-sha2-nistp256* | \
            ecdsa-sha2-nistp384* | \
            ecdsa-sha2-nistp521* | \
            ssh-ed25519* | \
            sk-ecdsa-sha2-nistp256@openssh.com* | \
            sk-ssh-ed25519@openssh.com*)
            ssh_keys="$(ssh-add -L)"

            case "$ssh_keys" in
                *$signing_key*) ;;

                *)
                    public_key_file="$(
                        grep --exclude "allowed_signers_file" \
                            --exclude "authorized_signatures" \
                            --files-with-matches --fixed-strings \
                            --recursive \
                            "$signing_key" \
                            ~/.ssh
                    )"
                    private_key_file=${public_key_file%.pub}

                    if test "$(uname)" = "Darwin"; then
                        extra_options='--apple-use-keychain'
                    fi

                    ssh-add -q $extra_options "$private_key_file"

                    ;;
            esac

            ;;
        *) ;;

    esac
fi
